import React,  { Component } from 'react'

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { persistor, store } from './src/store';

import { AppNavigation } from './src/navigation/AppNavigation'
import NavigatorService from './src/navigation/Navigator';
import {Root} from 'native-base';
import firebase from 'react-native-firebase';

export default class App extends Component {

    async componentDidMount(){
        this._retrieveRegistrationToken();
        this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
            // Process your token as required
        });

        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.messageListener = firebase.messaging().onMessage((message) => {
                // Process your message as required
            });
        } else {
            try {
                await firebase.messaging().requestPermission();
                alert("Grant Permission Notification")
                this.messageListener = firebase.messaging().onMessage((message) => {
                    // Process your message as required
                });
            } catch (error) {
                alert("Grant Permission Notification")
            }        
        }
    }


    async _handleListenNotification() {
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        });
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            // Process your notification as required
        });

        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification = notificationOpen.notification;
        });

        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            // App was opened by a notification
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification = notificationOpen.notification;
        }
    }

    _handleDisplayNotification(){
        // Build notification as above
        const notification = new firebase.notifications.Notification()
        .setNotificationId('notificationId')
        .setTitle('My notification title')
        .setBody('My notification body')
        .setChannelId('channelId')
        .setSmallIcon('ic_launcher')
        .setData({
            key1: 'value1',
            key2: 'value2',
        });

        // Display the notification
        firebase.notifications().displayNotification(notification)
    }

    async _retrieveRegistrationToken() {
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            // user has a device token
        } else {
            // user doesn't have a device token yet
        }
    }

    componentWillUnmount() {
        this.onTokenRefreshListener();
        this.messageListener();
    }

    render(){
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <Root>
                        <AppNavigation
                            ref={navigatorRef => {
                                NavigatorService.setContainer(navigatorRef);
                            }}
                        />
                    </Root>
                </PersistGate>
            </Provider>
        )
    }
    
}

