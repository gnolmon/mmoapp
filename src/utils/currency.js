import numeral from 'numeral'

export const currency_format = (value) => {
    return numeral(Number.parseFloat(value)).format('0,0');
}

