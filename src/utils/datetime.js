import moment from 'moment';

export const datetime_format = (value, format) => {
    if (value && value.toString().length > 10) {
        return moment.unix(value/1000).format(format)
    }
    return moment.unix(value).format(format);
}

export const date_ago = ( numdays, format) => {
    return moment().subtract(numdays, 'days').format(format);
}

export const date_now = (format) => {
    return moment().format(format);
}