import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native'
import { connect } from 'react-redux';

class SplashScreen extends React.Component {

    constructor(props) {
        super(props);
        const user = this.props.user;
        console.log('SplashScreen: ' + JSON.stringify(user));
        // if (user) {
        //     authNavigate(this.props.navigation, user.type);
        //     return;
        // }
        this.props.navigation.navigate('SignIn');
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const mapStateToProps = ({ auth }) => {
    const { user } = auth;
    return { user };
};

export default connect(mapStateToProps, null)(SplashScreen);


