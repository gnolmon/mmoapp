import React from 'react';
import {StyleSheet, Alert} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay';
import {connect} from 'react-redux';
import {loginUser} from "../actions";
import {
    Container, Content, View, Form, Item, Input, Button, Text
} from 'native-base';
import {RESPONSE_SUCCESS_CODE} from "../api/responseCode";

class SignInScreen extends React.Component {

    constructor(props) {
        super(props);
        const navigation = this.props.navigation;
        this.state = {
            phone: 'PH01', password: 'test123',
            loading: false,
            type: navigation.getParam('type', '')
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: `${navigation.getParam('title', 'Đăng nhập')}`,
        };
    };

    async onButtonPress() {
        const {phone, password, type} = this.state;
        if (type.length === 0) {
            return;
        }
        this.setState({loading: true});
        const result = await this.props.loginUser({phone, password, type});
        if (result.errorCode === RESPONSE_SUCCESS_CODE) {
            //authNavigate(this.props.navigation, result.data.type);
            return;
        }
        Alert.alert('Thông báo', result.message, [
            { text: 'OK', onPress: () => { this.setState({loading: false})}, style: 'cancel'}
        ], { cancelable: false });
    }

    render() {
        return (
            <Container>
                <Spinner visible={this.state.loading}/>
                <Content padder contentContainerStyle={styles.content}>
                    <View style={styles.container}>
                        <Text>This is SignInScreen</Text>
                    </View>
                </Content>
            </Container>
        );
    }
    componentWillUnmount() {
        this.setState({loading: false});
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF'
    },
    content: {
        paddingTop: 40
    },
    item: {
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10
    },
    logo: {
        width: 160,
        height: 160,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#ccc',
        backgroundColor: '#cacaca'
    },
    form: {
        alignItems: 'flex-start',
        paddingLeft: 10,
        paddingRight: 10
    },
    button: {
        marginBottom: 10
    }
});

export default connect(null, {loginUser})(SignInScreen);

