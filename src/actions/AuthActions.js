import loginApi from '../api/auth'

import { createActions } from 'redux-actions';
import {RESPONSE_SUCCESS_CODE} from "../api/responseCode";
export const {loginSuccess, loginFail, logout} = createActions('LOGIN_SUCCESS', 'LOGIN_FAIL', 'LOGOUT');

export const login = ({phone, password, type}) => {
    return async dispatch => {
        let response = await loginApi({phone, password, type});
        response.errorCode != RESPONSE_SUCCESS_CODE ? dispatch(loginFail()) : dispatch(loginSuccess(response.data));
        return response;
    };
};
