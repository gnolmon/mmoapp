import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import SplashScreen from '../containers/SplashScreen'
import SignInScreen from '../containers/SignInScreen'

export const AppNavigation = createSwitchNavigator(
    {
        Splash: SplashScreen,
        SignIn: SignInScreen
    },
    {
        initialRouteName: 'Splash',
        navigationOptions: {
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    }
);