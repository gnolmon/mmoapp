import { REHYDRATE } from 'redux-persist';
import { handleActions, combineActions } from 'redux-actions';
import NavigatorService from '../navigation/Navigator';

import {loginSuccess, loginFail, logout} from "../actions/AuthActions";

const INITIAL_STATE = {
    user: null
};

export default handleActions({
    [REHYDRATE](state, action) {
        return {...state, user: action.payload ? action.payload.user : null};
    },
    [loginSuccess](state, action) {
        return {...state, user: action.payload};
    },
    [loginFail](state) {
        return {...state, ...INITIAL_STATE};
    },
    [logout](state) {
        NavigatorService.navigate('SignIn');
        return {...state, ...INITIAL_STATE};
    }
}, INITIAL_STATE);