import { combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import AuthReducer from './AuthReducer';

const authPersistConfig = {
    key: 'auth',
    storage: storage,
    whitelist: ['user']
};

const appReducer = combineReducers({
    auth: persistReducer(authPersistConfig, AuthReducer),
});

const rootReducer = (state, action) => {
    if (action.type === 'LOGOUT') {
        Object.keys(state).forEach(key => {
            storage.removeItem(`persist:${key}`);
        });
        state = undefined;
    }
    return appReducer(state, action);
};

export default rootReducer;